resource "dns_a_record_set" "this" {
  zone = var.zone
  name = var.a_record
  addresses = var.ip
  ttl = 300
}

resource "dns_txt_record_set" "txtrec" {
  zone = var.zone
  txt = [
    var.txtrecord,
  ]
  ttl = 300
}