
variable username {
    description = "User name"
    type        = string
}
variable "userpass" {
    description = "User password"
    type        = string
}
variable mail {
    description = "User email"
    type        = string
}
variable "base_url" {
   type        = string
   description = "Base URL of Gitlab instance"
}
variable "gitlab_token" {
    type = string
    description = "Token of Gitlab instance"
}