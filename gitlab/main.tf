

module "gitlab-module" {
  source = "git::https://gitlab.com/keefeere/gitlab-module.git"
  username     = var.username
  userpass     = var.userpass
  mail         = var.mail
}


terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.4.0"
    }
  }
   required_version = ">= 0.13"
}

provider "gitlab" {
    base_url = var.base_url
    token = var.gitlab_token
}

terraform {
  backend "consul" {
     path    = "terraform/terraform-gitlab.tfstate"
  }
}
