data "terraform_remote_state" "gitlabstatedata" {
   backend = "consul" 
    config = {
     path    = "terraform/terraform-gitlab.tfstate"
     address = "http://debianvm:8500"
     scheme  = "http"
    }
    
  }
