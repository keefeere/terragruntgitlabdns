terraform {
  backend "consul" {
     path    = "terraform/terraform-dns.tfstate"
  }
}

provider "dns" {
  version = "~> 3.0"
  update {
    server        = "debianvm"
    key_name      = "dns.lab."
    key_algorithm = "hmac-md5"
    key_secret    = "t1/MeaIP+VMjkJJMEZEoZw=="
  }
}
