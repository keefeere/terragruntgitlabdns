
module "bindrec"{
  source = "../modules/dns-module"
  a_record = var.a_record
  zone = var.zone
  ip = var.ip
  txtrecord = data.terraform_remote_state.gitlabstatedata.outputs.project_url 
}