output "record_name" {
  value = module.bindrec.record_name
}

output "zone_name" {
  value = module.bindrec.dns_zone
}