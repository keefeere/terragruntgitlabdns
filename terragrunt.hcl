remote_state {
  backend = "consul"
  config = {
    address = "http://debianvm:8500"
    scheme  = "http"
    lock = true
  }
}
