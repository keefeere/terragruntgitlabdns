## **Homework 4**
## Terragrunt, external modules, remote state, output values, etc.

This repo contains two projects:
* Selfhosted gitlab provisioning with user, group and project
* DNS provision with simle A record and TXT record from GitLab state

**Features:**

- Common terragrunt config
- Module structure
- Get module from GitLab source
- Remote state storage in [Consul](http://consul.io)
- Pass secrets in **.tfvars**
- Get output values from remote state data source
  